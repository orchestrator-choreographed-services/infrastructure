## Start/Stop container

Use the provided shell scripts to start & stop required containers.
The required topics will be auto-created as part of this start script.

start the containers
./start.sh

### Provision workflow & connectors

Follow the following steps to configure required Kafka connectors and Zeebe workflow.

* cd Zeebe
* make workflow
* make command-signals-source
* make payment-received-sink
* make payment-checks-completed-sink

### Publish payments

To publish payments, please use the script `publish_payments.sh` and pass 
the payment ID. For example `./publish_payments.sh 1`

## Zeebe UI

Login to the UI  http://localhost:8080 (use demo/demo credentials)

## Zeebe Kafka connector & message format

Because of the message format the Zeebe connector is sending the body as string.
So we need to extract `paymentId` from the message payload that Zeebe connector publishes to Kafka.
To solve this issue I needed to use [kafka-connect-transform-common]: https://gitlab.com/Hussein.Moussa/kafka-connect-transform-common/blob/master/src/main/java/com/github/jcustenborder/kafka/connect/transform/common/ExtractZeebeKey.java
to extract the paymentId correlation ID from the command signal payload. 
