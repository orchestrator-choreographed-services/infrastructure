#!/usr/bin/env bash

docker-compose down
docker volume prune -f
rm -rf /tmp/kafka-streams/