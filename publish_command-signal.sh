#!/usr/bin/env bash

curl --request POST \
  --url http://127.0.0.1:8082/topics/tw-payments.command-signal.fraud-check \
  --header 'accept: application/vnd.kafka.v2+json, application/vnd.kafka+json, application/json' \
  --header 'content-type: application/vnd.kafka.json.v2+json' \
  --data '{
	"records": [
		{
			"key": "payment_1",
			"value": {
				"processingType": "DowngradedPayment"
			}
		}
	]
}'

curl --request POST \
  --url http://127.0.0.1:8082/topics/tw-payments.command-signal.limit \
  --header 'accept: application/vnd.kafka.v2+json, application/vnd.kafka+json, application/json' \
  --header 'content-type: application/vnd.kafka.json.v2+json' \
  --data '{
	"records": [
		{
			"key": "payment_1",
			"value": {}
		}
	]
}'