#!/usr/bin/env bash

paymentId=$1

docker-compose exec zeebe zbctl create instance --variables "{\"paymentId\": $paymentId}" payments

curl --request POST \
  --url http://localhost:8082/topics/tw-payments.payment-received \
  --header 'accept: application/vnd.kafka.v2+json, application/vnd.kafka+json, application/json' \
  --header 'content-type: application/vnd.kafka.json.v2+json' \
  --data '{
	"records": [
		{
			"key": '$paymentId',
			"value": {
				"fromAccount": "From account 1",
				"toAccount": "To account 1",
				"amount": "100.0",
				"currency": "AUD",
				"paymentId": '$paymentId',
				"eventType": "PaymentReceived"
			}
		}
	]
}'